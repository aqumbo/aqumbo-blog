+++
date = "2017-10-29T11:30:37+02:00"
title = "About"
menu = "main"
+++

#### Ways to contact me
<i class="fa fa-envelope"></i> [alex@aqumbo.se](mailto:alex@aqumbo.se)

<i class="fa fa-reddit"></i> [@couchquid](https://www.reddit.com/u/couchquid)

<i class="fa fa-gitlab"></i> [Gitlab](https://www.gitlab.com/couchquid)

<i class="fa fa-terminal"></i> couchquid @ Freenode
